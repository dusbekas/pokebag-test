const linkMain = 'https://pokeapi.co/api/v2/';

const linkPokemonImg = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/'
    'sprites/pokemon/';
const linkPokedex = 'pokedex/1/';
const linkPokemon = 'pokemon/';


const imgPokeBall = 'assets/pokeball.webp';


const hivePokebag = 'hivePokeBag';
const hivePokebagMod = 'hivePokeBagMod';