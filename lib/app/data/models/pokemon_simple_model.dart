import 'package:hive/hive.dart';

part 'pokemon_simple_model.g.dart';

@HiveType(typeId: 0)
class PokemonSimple {
  PokemonSimple({
    required this.entry,
    required this.name,
    this.nickName,
  });

  @HiveField(0)
  int entry;

  @HiveField(1)
  String name;

  @HiveField(2)
  String? nickName;
}

@HiveType(typeId: 1)
class PokemonSimpleList {
  PokemonSimpleList({
    this.list,
  });

  @HiveField(0)
  List<PokemonSimple>? list;
}
