
import 'dart:convert';

class Pokedex {
  List<Descriptions>? descriptions;
  int? id;
  bool? isMainSeries;
  String? name;
  List<Names>? names;
  List<PokemonEntries>? pokemonEntries;
  dynamic region;
  // List<Null>? versionGroups;

  Pokedex(
      {this.descriptions,
      this.id,
      this.isMainSeries,
      this.name,
      this.names,
      this.pokemonEntries,
      this.region,
      // this.versionGroups
      });

  Pokedex.fromJson(Map<String, dynamic> json) {
    if (json['descriptions'] != null) {
      descriptions = <Descriptions>[];
      json['descriptions'].forEach((v) {
        descriptions?.add(Descriptions.fromJson(v));
      });
    }
    id = json['id'];
    isMainSeries = json['is_main_series'];
    name = json['name'];
    if (json['names'] != null) {
      names = <Names>[];
      json['names'].forEach((v) {
        names?.add(Names.fromJson(v));
      });
    }
    if (json['pokemon_entries'] != null) {
      pokemonEntries = <PokemonEntries>[];
      json['pokemon_entries'].forEach((v) {
        pokemonEntries?.add(PokemonEntries.fromJson(v));
      });
    }
    region = json['region'];
    /*if (json['version_groups'] != null) {
      versionGroups = <Null>[];
      json['version_groups'].forEach((v) {
        versionGroups?.add(Null.fromJson(v));
      });
    }*/
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (descriptions != null) {
      data['descriptions'] = descriptions?.map((v) => v.toJson()).toList();
    }
    data['id'] = id;
    data['is_main_series'] = isMainSeries;
    data['name'] = name;
    if (names != null) {
      data['names'] = names?.map((v) => v.toJson()).toList();
    }
    if (pokemonEntries != null) {
      data['pokemon_entries'] = pokemonEntries?.map((v) => v.toJson()).toList();
    }
    data['region'] = region;
    /*if (versionGroups != null) {
      data['version_groups'] = versionGroups?.map((v) => v.toJson()).toList();
    }*/
    return data;
  }
}

class Descriptions {
  String? description;
  Language? language;

  Descriptions({this.description, this.language});

  Descriptions.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    language =
        json['language'] != null ? Language?.fromJson(json['language']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['description'] = description;
    if (language != null) {
      data['language'] = language?.toJson();
    }
    return data;
  }
}

class Language {
  String? name;
  String? url;

  Language({this.name, this.url});

  Language.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['url'] = url;
    return data;
  }
}

class Names {
  Language? language;
  String? name;

  Names({this.language, this.name});

  Names.fromJson(Map<String, dynamic> json) {
    language =
        json['language'] != null ? Language?.fromJson(json['language']) : null;
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (language != null) {
      data['language'] = language?.toJson();
    }
    data['name'] = name;
    return data;
  }
}

class PokemonEntries {
  int? entryNumber;
  Language? pokemonSpecies;

  PokemonEntries({this.entryNumber, this.pokemonSpecies});

  PokemonEntries.fromJson(Map<String, dynamic> json) {
    entryNumber = json['entry_number'];
    pokemonSpecies = json['pokemon_species'] != null
        ? Language?.fromJson(json['pokemon_species'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['entry_number'] = entryNumber;
    if (pokemonSpecies != null) {
      data['pokemon_species'] = pokemonSpecies?.toJson();
    }
    return data;
  }
}
