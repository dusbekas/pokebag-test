// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pokemon_simple_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PokemonSimpleAdapter extends TypeAdapter<PokemonSimple> {
  @override
  final int typeId = 0;

  @override
  PokemonSimple read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PokemonSimple(
      entry: fields[0] as int,
      name: fields[1] as String,
      nickName: fields[2] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, PokemonSimple obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.entry)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.nickName);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PokemonSimpleAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class PokemonSimpleListAdapter extends TypeAdapter<PokemonSimpleList> {
  @override
  final int typeId = 1;

  @override
  PokemonSimpleList read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PokemonSimpleList(
      list: (fields[0] as List?)?.cast<PokemonSimple>(),
    );
  }

  @override
  void write(BinaryWriter writer, PokemonSimpleList obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.list);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PokemonSimpleListAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
