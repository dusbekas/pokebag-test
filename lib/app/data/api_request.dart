import 'dart:async';
import 'package:dio/dio.dart' as dio;
import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:hive/hive.dart';
import 'package:pokebag_test/app/data/models/pokedex_model.dart';
import 'package:pokebag_test/app/data/models/pokemon_model.dart';
import 'package:pokebag_test/app/data/models/pokemon_simple_model.dart';
import 'package:pokebag_test/constant_value/constant.dart';

class ApiRequest {
  static var client = dio.Dio();

  static Future<Pokedex?> fetchPokedex() async {
    String url = linkMain + linkPokedex;
    try {
      dio.Response response = await client
          .get(url)
          .timeout(const Duration(seconds: 30));

      if (response.statusCode == HttpStatus.ok) {
        return Pokedex.fromJson(response.data);
      // TODO: Handle error
      } else {
        return null;
      }
    } on TimeoutException catch (_) {
      return null;
    }
  }

  static Future<Pokemon?> fetchPokemon(String entry) async {
    String url = linkMain + linkPokemon + entry;
    try {
      dio.Response response = await client
          .get(url)
          .timeout(const Duration(seconds: 30));

      if (response.statusCode == HttpStatus.ok) {
        return Pokemon.fromJson(response.data);
        // TODO: Handle error
      } else {
        return null;
      }
    } on TimeoutException catch (_) {
      return null;
    }
  }

  static Future<PokemonSimpleList?> fetchPokebag() async {
    var pokebag = Hive.box<PokemonSimpleList>(hivePokebag);

    return pokebag.get(hivePokebagMod);
  }

  static Future<void> addPokemonToBag(PokemonSimple pokemon) async {
    var pokebag = Hive.box<PokemonSimpleList>(hivePokebag);
    final pokemonList = pokebag.get(hivePokebagMod);

    List<PokemonSimple> newPokemonList = [];
    final newPokemon = PokemonSimple(
        entry: pokemon.entry,
        name: pokemon.name,
        nickName: pokemon.nickName,
    );

    if (pokemonList == null) {
      newPokemonList.add(newPokemon);
    } else {
      newPokemonList.addAll(pokemonList.list!);
      newPokemonList.add(newPokemon);
    }

    await pokebag.put(
      hivePokebagMod,
      PokemonSimpleList(list: newPokemonList),
    );
  }

  static Future<void> removePokemonFromBag(PokemonSimple pokemon) async {
    var pokebag = Hive.box<PokemonSimpleList>(hivePokebag);
    final pokemonList = pokebag.get(hivePokebagMod);

    List<PokemonSimple> newPokemonList = [];

    newPokemonList.addAll(pokemonList!.list!);
    newPokemonList.remove(pokemon);

    await pokebag.put(
      hivePokebagMod,
      PokemonSimpleList(list: newPokemonList),
    );
  }
}

