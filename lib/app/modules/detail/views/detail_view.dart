import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:pokebag_test/app/routes/app_pages.dart';
import 'package:pokebag_test/constant_value/constant.dart';
import 'package:simple_tags/simple_tags.dart';
import 'package:spring/spring.dart';

import '../controllers/detail_controller.dart';

class DetailView extends GetView<DetailController> {
  const DetailView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final SpringController springController =
        SpringController(initialAnim: Motion.mirror);
    final DetailController controller = Get.put(DetailController());
    var size = MediaQuery.of(context).size;

    /*return Scaffold(
      appBar: AppBar(
        title: Text('DetailView'),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          Get.parameters['user'] ?? '',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );*/
    return Scaffold(
        floatingActionButton: Obx(
          () => controller.loading.value
              ? const SizedBox()
              : Spring.rotate(
                  springController: springController,
                  alignment: Alignment.bottomCenter, //def=center
                  startAngle: -15, //def=0
                  endAngle: 45, //def=360
                  animDuration: Duration(seconds: 2), //def=1s
                  curve: Curves.easeInBack, //def=Curves.easInOut
                  child: FloatingActionButton(
                    onPressed: () {
                      controller.catchPokemon(context);
                    },
                    child: Container(
                      width: size.width / 3,
                      height: size.width / 3,
                      child: Image.asset(imgPokeBall),
                    ),
                  ),
                ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        appBar: AppBar(
          title: Text(controller.loading.value
              ? 'Pokédex'
              : controller.pokemon?.name ?? ''),
          centerTitle: true,
          actions: [
            Padding(
              padding: const EdgeInsets.all(8),
              child: InkWell(
                onTap: () => Get.toNamed(Routes.POKEBAG),
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  child: Text('Pokébag'),
                ),
              ),
            )
          ],
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.fetchPokemon(Get.parameters['entry'] ?? '');
          },
          child: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Obx(
              () => controller.loading.value
                  ? const Center(
                      child: CircularProgressIndicator(),
                    )
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Stack(
                          children: [
                            CachedNetworkImage(
                              imageUrl: linkPokemonImg +
                                  controller.pokemon!.id.toString() +
                                  '.png',
                              width: size.width,
                              height: size.width / 2,
                              fit: BoxFit.contain,
                            ),
                            Positioned(
                              right: 8,
                              bottom: 0,
                              child: _items(controller.types),
                            ),
                          ],
                        ),
                        SizedBox(height: 16),
                        Container(
                          width: double.infinity,
                          child: Card(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(height: 8),
                                  Text(
                                    'Abilities',
                                    style: TextStyle(fontSize: 24),
                                  ),
                                  SizedBox(height: 16),
                                  _items(controller.abilities),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 16),
                        Container(
                          width: double.infinity,
                          child: Card(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(height: 8),
                                  Text(
                                    'Moves',
                                    style: TextStyle(fontSize: 24),
                                  ),
                                  SizedBox(height: 16),
                                  _items(controller.moves),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 64),
                      ],
                    ),
            ),
          ),
        ));
  }

  Widget _items(List<String> content) {
    return SimpleTags(
      content: content,
      wrapSpacing: 8,
      wrapRunSpacing: 8,
      tagContainerPadding: const EdgeInsets.all(8),
      tagContainerDecoration: BoxDecoration(
        color: Colors.transparent,
        border: Border.all(color: Colors.grey),
        borderRadius: const BorderRadius.all(
          Radius.circular(16),
        ),
      ),
    );
  }
}
