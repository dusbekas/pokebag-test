import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokebag_test/app/data/api_request.dart';
import 'package:pokebag_test/app/data/models/pokemon_model.dart';
import 'package:pokebag_test/app/data/models/pokemon_simple_model.dart';

class DetailController extends GetxController {
  Pokemon? pokemon = Pokemon();
  List<String> types = [];
  List<String> moves = [];
  List<String> abilities = [];
  var loading = true.obs;

  @override
  void onInit() {
    super.onInit();
    fetchPokemon(Get.parameters['entry'] ?? '');
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<void> fetchPokemon(String entry) async {
    loading.value = true;
    pokemon = null;
    final getItems = await ApiRequest.fetchPokemon(entry);
    loading.value = false;
    if (getItems != null) {
      pokemon = getItems;
      for (var element in pokemon!.types!) {
        types.add(element.type!.name ?? '');
      }
      for (var element in pokemon!.moves!) {
        moves.add(element.move!.name ?? '');
      }
      if (pokemon!.abilities != null) {
        for (var element in pokemon!.abilities!) {
          abilities.add(element.ability!.name ?? '');
        }
      }
    }
  }

  void catchPokemon(BuildContext context) async {
    // TODO: Pokemon rate
    Random random = Random();
    double probability = .7;
    double nextProbability = random.nextDouble();
    bool caught = nextProbability < probability;

    ApiRequest.addPokemonToBag(PokemonSimple(
      entry: pokemon!.id!,
      name: pokemon!.name!,
    ));

    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      duration: Duration(seconds: 30),
      content: Text(caught
          ? 'Gotcha! ' + pokemon!.name! + 'was caught'
          : 'Gah! It was so close'),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {},
      ),
    ));
    print(probability);
    print(nextProbability);
    print(caught);

    /*final getItems = await ApiRequest.addPokemonToBag(
      PokemonSimple(entry: pokemon!.id!, name: pokemon!.name!,)
    );
    */
  }
}
