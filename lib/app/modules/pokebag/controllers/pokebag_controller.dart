import 'dart:math';

import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:pokebag_test/app/data/api_request.dart';
import 'package:pokebag_test/app/data/models/pokemon_model.dart';
import 'package:pokebag_test/app/data/models/pokemon_simple_model.dart';

class PokebagController extends GetxController {
  PokemonSimpleList? items;
  var loading = true.obs;
  //TODO: Implement PokebagController

  @override
  void onInit() {
    super.onInit();

    fetchPokebag();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<void> fetchPokebag() async {
    loading.value = true;
    items = null;
    final getItems = await ApiRequest.fetchPokebag();
    loading.value = false;

    items = getItems;
  }

  void removePokemon(
    BuildContext context,
    PokemonSimple pokemonSimple,
  ) async {
    ApiRequest.removePokemonFromBag(pokemonSimple);
    fetchPokebag();
  }
}
