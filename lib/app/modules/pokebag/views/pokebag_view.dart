import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:pokebag_test/app/routes/app_pages.dart';
import 'package:pokebag_test/constant_value/constant.dart';

import '../controllers/pokebag_controller.dart';

class PokebagView extends GetView<PokebagController> {
  @override
  Widget build(BuildContext context) {
    final PokebagController controller = Get.put(PokebagController());
    var size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Pokébag'),
        centerTitle: true,
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          controller.fetchPokebag();
        },
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
          child: Obx(
            () => controller.loading.value
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : controller.items == null
                    ? const Center(
                        child: Text('Pokébag is empty'),
                      )
                    : ListView.builder(
                        itemCount: controller.items!.list!.length,
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          return Card(
                            margin: const EdgeInsets.only(bottom: 8),
                            child: InkWell(
                              onTap: () {
                                Get.toNamed(Routes.DETAIL +
                                    '/' +
                                    controller.items!.list![index].entry
                                        .toString());
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8),
                                child: Stack(
                                  children: [
                                    Row(
                                      children: [
                                        CachedNetworkImage(
                                          imageUrl: linkPokemonImg +
                                              controller.items!.list![index].entry
                                                  .toString() +
                                              '.png',
                                          width: size.width / 4,
                                          height: size.width / 4,
                                        ),
                                        const SizedBox(width: 16),
                                        Column(
                                          children: [
                                            Text(
                                              controller.items!.list![index].name,
                                            ),
                                            Text(
                                              controller.items!.list![index]
                                                      .nickName ??
                                                  '',
                                              style: const TextStyle(fontSize: 10),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                    Positioned(
                                      top: 0,
                                      bottom: 0,
                                      right: 0,
                                      child: IconButton(
                                        onPressed: () =>
                                            controller.removePokemon(context,
                                                controller.items!.list![index]),
                                        icon: Icon(Icons.delete_forever),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }),
          ),
        ),
      ),
    );
  }
}
