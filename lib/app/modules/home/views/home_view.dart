import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:pokebag_test/app/routes/app_pages.dart';
import 'package:pokebag_test/constant_value/constant.dart';

import '../controllers/home_controller.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final HomeController controller = Get.put(HomeController());
    var size = MediaQuery.of(context).size;

    return Scaffold(
        appBar: AppBar(
          title: const Text('Pokédex'),
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Get.changeTheme(
                Get.isDarkMode ? ThemeData.light() : ThemeData.dark(),
              );
            },
            icon: const Icon(Icons.brightness_6),
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.all(8),
              child: InkWell(
                onTap: () => Get.toNamed(Routes.POKEBAG),
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  child: Text('Pokébag'),
                ),
              ),
            )
          ],
          /*actions: [
            IconButton(
              onPressed: () {
                Get.changeTheme(
                  Get.isDarkMode ? ThemeData.light() : ThemeData.dark(),
                );
              },
              icon: const Icon(Icons.brightness_6),
            )
          ],*/
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            controller.fetchPokedex();
          },
          child: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
            child: Obx(
              () => controller.loading.value
                  ? const Center(
                      child: CircularProgressIndicator(),
                    )
                  : ListView.builder(
                      itemCount: controller.itemItems!.pokemonEntries!.length,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return Card(
                          margin: const EdgeInsets.only(bottom: 8),
                          child: InkWell(
                            onTap: () {
                              Get.toNamed(Routes.DETAIL +
                                  '/' +
                                  controller.itemItems!.pokemonEntries![index]
                                      .entryNumber
                                      .toString());
                            },
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8),
                              child: Row(
                                children: [
                                  CachedNetworkImage(
                                    imageUrl: linkPokemonImg +
                                        controller.itemItems!
                                            .pokemonEntries![index].entryNumber
                                            .toString() +
                                        '.png',
                                    width: size.width / 4,
                                    height: size.width / 4,
                                  ),
                                  const SizedBox(width: 16),
                                  Text(
                                    controller.itemItems!.pokemonEntries![index]
                                            .pokemonSpecies?.name ??
                                        '',
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
            ),
          ),
        ));
  }
}
