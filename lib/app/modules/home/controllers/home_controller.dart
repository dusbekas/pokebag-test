import 'package:get/get.dart';
import 'package:pokebag_test/app/data/api_request.dart';
import 'package:pokebag_test/app/data/models/pokedex_model.dart';

class HomeController extends GetxController {
  Pokedex? itemItems = Pokedex();
  var loading = true.obs;

  @override
  void onInit() {
    super.onInit();

    fetchPokedex();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<void> fetchPokedex() async {
    loading.value = true;
    itemItems = null;
    final getItems = await ApiRequest.fetchPokedex();
    loading.value = false;
    if (getItems != null) {
      itemItems = getItems;
    }
  }
}
